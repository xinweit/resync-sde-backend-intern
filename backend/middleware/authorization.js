const jwt = require("jsonwebtoken");
require("dotenv").config();

// function to ensure user is logged in when calling the API endpoints 
module.exports = function(req, res, next) {
    try {
		// get token from header
		const jwtToken = req.header("token");

		// if no token return 403 error
		if (!jwtToken) {
			return res.status(403).json("Not Authorized");
		}

		// verify token is valid, otherwise throw 403 error
		const payload = jwt.verify(jwtToken, process.env.jwtSecret);

		req.user = payload.user;

		next();
	} catch (error) {
		console.error(error.message);
		return res.status(403).json("Not Authorized");
	}
};
