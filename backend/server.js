const express = require("express");
const app = express();

const pool = require("./db");
const cors = require("cors")

// environment variables
require("dotenv").config();
let host = process.env.HOST;
let port = process.env.PORT;

// req.body
app.use(express.urlencoded({extended: true})); 
app.use(express.json());   
app.use(cors());

// ROUTES

// register and login as user
app.use("/auth", require("./routes/auth"));

// manage employees
app.use("/employees", require("./routes/employees"))

app.listen(port, () => {
    console.log(`Server is listening on http://${host}:${port}`);
})