const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");

// add employee's details into database
router.post("/addEmployee", authorization, async (req, res) => {
    try {
        const { id, name, job_title, job_type, salary } = req.body;
        const addEmployeeDetails = await pool.query(
            "INSERT INTO employees(id, name, job_title, job_type, salary) VALUES($1, $2, $3, $4, $5)",
            [id, name, job_title, job_type, salary]
        );
        res.json("Successfully added employee's details");
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
});


// get an employee's details
router.get("/getEmployee/:e_id", authorization, async (req, res) => {
    try {
        const employeeId = req.params.e_id 
        const getEmployeeDetails = await pool.query(
            "SELECT id, name, job_title, job_type, salary FROM employees WHERE id = $1",
            [employeeId]
        );
        res.json(getEmployeeDetails.rows);
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
});



// get a list of all employees
router.get("/getEmployees", authorization, async (req, res) => {
    try {
        const getAllEmployeeDetails = await pool.query(
            "SELECT id, name, job_title, job_type, salary FROM employees"
        );
        res.json(getAllEmployeeDetails.rows);
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
});

// update employee's details
router.put("/updateEmployee/:e_id", authorization, async (req, res) => {
    try {
        const employeeId = req.params.e_id 
        const { id, name, job_title, job_type, salary } = req.body;
        const updateEmployeeDetails = await pool.query(
            "UPDATE employees SET id = $1, name = $2, job_title = $3, job_type = $4, salary = $5 WHERE id = $6",
            [id, name, job_title, job_type, salary, employeeId]
        );
        res.json("Successfully updated employee's details");
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
});

// delete an employee's details
router.delete("/deleteEmployee/:e_id", authorization, async (req, res) => {
    try {
        const employeeId = req.params.e_id
        const deleteEmployeeDetails = await pool.query(
            "DELETE FROM employees WHERE id = $1", 
            [employeeId]
        );
        res.json("Successfully deleted employee's details");
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
});

module.exports = router;