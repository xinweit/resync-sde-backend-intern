const router = require("express").Router();
const pool = require("../db");
const bcrypt = require("bcrypt");
const jwtGenerator = require("../utils/jwtGenerator");
//dd
// register as a user
router.post('/register', async (req, res) => {
    try {
        // get details from req.body
        const { username, password } = req.body;

        // check if user already exists, throw error if true
        const user = await pool.query("SELECT * FROM users WHERE username = $1", [username]);

        if (user.rows.length !== 0) {
            return res.status(401).send("User already exists");
        }

        // bcrypt user password
        const saltRounds = 10;
        const salt = await bcrypt.genSalt(saltRounds);
        const bcryptPassword = await bcrypt.hash(password, salt);

        // save new user in database
        const newUser = await pool.query(
            "INSERT INTO users (username, password) VALUES ($1, $2) RETURNING *",
            [username, bcryptPassword]
        );

        res.json("Successfully added user");
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
})

// login as a user
router.post("/login", async (req, res) => {
    try {

        // get details from req.body
        const { username, password } = req.body;

        // check if user exists, throw error if false
        const user = await pool.query("SELECT * FROM users WHERE username = $1", [
            username
        ]);

        if (user.rows.length === 0) {
            return res.status(401).json("Invalid username or password");
        }

        // check that password entered matches saved password in database
        const isValidPassword = await bcrypt.compare(
            password,
            user.rows[0].password
        );

        if (!isValidPassword) {
            return res.status(401).json("Invalid username or password");
        }

        // output jwt token
        const jwtToken = jwtGenerator(user.rows[0].username);
        res.json({ jwtToken });
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
    }
});

module.exports = router;