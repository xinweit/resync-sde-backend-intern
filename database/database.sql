CREATE DATABASE resync;

CREATE TABLE employees (
    id INTEGER PRIMARY KEY,
    name VARCHAR(255),
    job_title VARCHAR(255),
    job_type VARCHAR(255),
    salary INTEGER
);

CREATE TABLE users (
    username VARCHAR(255) PRIMARY KEY,
    password VARCHAR(255) NOT NULL
);
