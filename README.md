# Guide

 Back-end application that returns an Restful API for employee management.

<br>

## Setup instructions:

1. Clone the repository into a directory of your choice.
2. Install the following packages in the backend directory:  
npm install express  
npm install pg  
npm install nodemon  
npm install cors  
npm install dotenv  
npm install bcrypt  
npm install jsonwebtoken
3. Ensure PostgreSQL is installed.
4. Create the database and tables in the database.sql file in PostgreSQL terminal as user 'postgres'.
5. Ensure that Postman is installed.

<br>

## Tables in PostgreSQL database
The table names and column names in the resync database are:  
<br>
__users__  
username  
password  
<br>
__employees__  
id  
name  
job_title  
job_type  
salary  
<br>

## Usage instructions:

Run 'nodemon server' or 'npm start' in the backend directory.
The server will listen on port 5000 by default.
Send the API requests using Postman.

<br>

### Register as a user

Register as a user using the route http://localhost:5000/auth/register with a POST request. Include the username and password you want to create in the request body. The password will be hashed using bcrypt and saved with the username into the users table in the database. If the username already exists a "User already exists" 401 error will be thrown.

![register_image](images/register.png)

<br>

### Login as a user

Login as a user using the route http://localhost:5000/auth/login with a POST request. Include your username and password in the request body. A Json Web Token will be sent back in the response body if the credentials are correct. Invalid credentials provided will result in a "Invalid username or password" 401 error.


![login_image](images/login.png)

<br>

### Json Web Token

Add the jwt token generated from login to the headers of subsequent requests to the API, as shown in the picture below. When the jwt token provided is invalid, it will result in a "Not Authorized" 403 error. The jwt token is set to expire in 1 hour by default.

![jwtToken_image](images/jwtToken.png)

<br>

### Add an employee

To add an employee to the employees table in the database, use http://localhost:5000/employees/addEmployee with a POST request. You can include the id, name, job title, job type and salary of the employee in the request body. 

![add_image](images/add.png)

<br>

### Get an employee 

To get the details of an employee, use http://localhost:5000/employees/getEmployee/[id] where [id] is the id of the employee that you want to retrieve with a GET request. The specified employee's details will be sent back in the response body.
![getOne_image](images/getOne.png)

<br>

### Get a list of all employees

To get a list of all the employees, use http://localhost:5000/employees/getEmployees with a GET request.
A list of all employees' details will be sent back in the response body. 

![getAll_image](images/getAll.png)

<br>

### Update an employee

To update an employee, use http://localhost:5000/employees/updateEmployee/[id] where [id] is the id of the employee you want to update with a PUT request.

![update_image](images/update.png)

<br>

### Delete an employee

To delete an employee, use http://localhost:5000/employees/deleteEmployee/[id] where [id] is the id of the employee you want to delete with a DELETE request.

![delete_image](images/delete.png)

<br>


